# Alliance Auth Apps

This is the repository for the website "Alliance Auth Apps".

[![release](https://img.shields.io/badge/release-1.9.5-orange)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory)
[![python](https://img.shields.io/badge/python-3.11-blue)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory)
[![django](https://img.shields.io/badge/django-4.2-blue)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory)
[![pipeline](https://gitlab.com/ErikKalkoken/allianceauth-app-directory/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory/-/pipelines)
[![coverage report](https://gitlab.com/ErikKalkoken/allianceauth-app-directory/badges/master/coverage.svg)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory/-/commits/master)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitlab.com/ErikKalkoken/allianceauth-app-directory/-/blob/master/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Contents

- [Overview](#overview)
- [Key Features](#key-features)
- [Installation](#installation-guide)
- [Contributions](#contributions)
- [Change Log](CHANGELOG.md)

## Overview

The purpose of this website is:

- Allow users to quickly find the Allianceauth community app they are looking for
- Allow users to discover new and interesting apps
- Allow users to give feedback on apps

The live website canbe found here: [Alliance Auth Apps](https://apps.allianceauth.org/)

## Key features

- Users can browse the app catalog by category
- Users can search the app catalog by key word
- Users can see all relevant details of an apps at a glance
- Users can sign-up with their Eve accounts
- Users can submit reviews for apps (required login)
- Users can submit their apps to the catalog (requires login)
- App maintainers can post replies to reviews

## Installation guide

This guide is for installing this website for production use on Ubuntu 16.04+.

### Linux Distribution

You need the following packages installed on Ubuntu:

```shell
sudo apt install build-essentials libmysqlclient-dev
```

### Eve Online app

Create a Eve app on developer.eveonline.com.

- Type: Authentication only
- Callback: `https://my-app.example.com/sso/callback`

### Database

Create a database for the app on mysql:

```sql
CREATE USER 'allianceauth_appdir'@'localhost' IDENTIFIED BY 'PASSWORD';
CREATE DATABASE allianceauth_appdir CHARACTER SET utf8mb4;
GRANT ALL PRIVILEGES ON allianceauth_appdir . * TO 'allianceauth_appdir'@'localhost';
```

### OS

Install Python 3.11:

```shell
sudo apt install python3.11 python3.11-dev python3.11-venv
```

Create a dedicated user for the website:

```shell
sudo adduser --disabled-login appdir
```

### Django server

Switch to the new user and create a venv in the home folder:

```shell
sudo su appdir
cd ~
python3.11 -m venv venv
source venv/bin/activate
pip install -U pip setuptools
pip install wheel
git clone https://gitlab.com/ErikKalkoken/allianceauth-app-directory.git appdir
cd appdir
pip install -r requirements.txt
```

Create the local settings by copying and modifying the example file:

```shell
cd appdir/settings
cp local_example.py local.py
```

Create the local urls file by copying and modifying the example file:

```shell
cd ../urls
cp local_example.py local.py
```

Install additional packages for production:

```shell
pip install mysqlclient
pip install gunicorn
```

Create the folder for the static files and give ownership to the Django app:

```shell
sudo mkdir /var/www/appdir
chown appdir:appdir /var/www/appdir
```

Run migrations & collectstatic

```shell
python manage.py migrate
python manage.py collectstatic
```

### Supervisor

Setup the Django server to run via supervisor.

Create a configuration file for supervisor named `appdir_supervisor.conf` in `/home/appdir/appdir`:

```config
[program:appdir]
directory=/home/appdir/appdir
command=/home/appdir/venv/bin/gunicorn --bind 0.0.0.0:8200 appdir.wsgi
user=appdir
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
stderr_logfile=/home/appdir/appdir/log/gunicorn.err.log
stdout_logfile=/home/appdir/appdir/log/gunicorn.out.log
```

Add the new configuration to supervisor by creating a symbolic link and enable the configuration:

```shell
sudo ln -s /home/appdir/appdir/appdir_supervisor.conf /etc/supervisor/conf.d/
sudo supervisorctl reload
```

### NGINX

Appdir specific configuration:

```nginx
# Ignore missing favicon
location = /favicon.ico { access_log off; log_not_found off; }

# Alias for static files
location /static {
    alias /var/www/myapp/static;
    autoindex off;
}

# Set gunicorn as proxy
location / {
    include proxy_params;
    proxy_pass http://127.0.0.1:8000;
}
```

### Apache2

```apache
<VirtualHost *:80>
    ServerName allianceauth-apps.local # Change to your URL

    ProxyPassMatch ^/static !
    ProxyPassMatch ^/robots.txt !

    ProxyPass / http://127.0.0.1:8000/
    ProxyPassReverse / http://127.0.0.1:8000/

    ProxyPreserveHost On

    Alias "/robots.txt" "/var/www/myapp/static/robots.txt"
    Alias "/static" "/var/www/myapp/static/"

    <Directory "/var/www/myapp/static/">
        Require all granted
    </Directory>

    <Location "/robots.txt">
        SetHandler None
        Require all granted
    </Location>
</VirtualHost>
```

## Contributions

This website is open source. Contributions and feedback are very welcome!
