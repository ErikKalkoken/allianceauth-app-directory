from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include("catalog.urls")),
    path("admin/", admin.site.urls),
    path("sso/", include("esi.urls", namespace="esi")),
    path("eve_auth/", include("eve_auth.urls")),
]
