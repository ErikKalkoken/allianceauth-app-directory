# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## UNRELEASED

## [1.15.1] - 2024-03-09

## Fixed

- Can not handle PyPI entries without summary

## [1.15.0] - 2024-11-25

## Added

- Ability to blacklist apps

## [1.14.0] - 2023-12-27

## Added

- New filter for AA 4 compatible apps
- Now shows classifiers for apps on detail tab

## [1.12.0] - 2023-11-18

## Added

- Filter apps list by author in sidebar
- Show app counts for filter categories in sidebar
- Optional mapping of author names, e.g. to consolidate spelling variants

## Changed

- Improved test suite

## Fixed

- Dependencies page now runs faster

## [1.11.0] - 2023-11-18

## Changed

- Show multiple authors for apps

## Fixed

- Homepage, license and authors not shown for pep621 projects

## [1.10.1] - 2023-11-17

### Fixed

- Fetching author not working for pep621 style projects

## [1.10.0] - 2023-10-13

### Change

- Replaced author filter by maintainer filter, because PyPI does no longer provide accurate information about the app's author

## [1.9.5] - 2023-08-31

### Change

- Add pylint checks

## [1.9.4] - 2023-08-31

### Change

- Upgrade to Django 4.2

## [1.9.3] - 2023-08-31

### Change

- Upgrade to Python 3.11
- Remove overzealous version pinning

## [1.9.2] - 2023-08-31

### Fix

- Can not add an app with a very large license field

## [1.9.1] - 2023-01-07

### Fix

- Indented code blocks are not correctly formatted (e.g. aa-freight, installation, celery snippet)

## [1.9.0] - 2023-01-06

### Changed

- Show date of when app was first published on PyPI instead of added to the website

## [1.8.0] - 2023-01-06

### Added

- Export apps data as CSV via admin site

## [1.7.1] - 2022-09-13

### Fixes

- Bootstrap and other fixes (!10)

## [1.7.0] - 2022-06-27

### Added

- Show app dependencies as network diagram

## [1.6.0] - 2022-06-13

### Added

- Show which apps are required and which apps are using an app on detail page

### Fixed

- Do not show empty authors in sidebar

## [1.5.0] - 2022-06-13

### Added

- Show requiremments for apps

## [1.4.0] - 2022-06-13

### Added

- Code highlighting (Thanks to @ppfeufer for the contribution!)
- Code highlighting in light and dark mode

## [1.3.1] - 2022-06-04

### Fixed

- Error 500 on app list page for users that are not logged in

## [1.3.0] - 2022-06-02

### Added

- Automatically detect and add new apps added to the official [Community Creations](https://gitlab.com/allianceauth/community-creations) list
- New feature: Show newly added apps
- Show counts for new apps and my apps in sidebar

## [1.2.0] - 2022-02-01

### Added

- Command for mass importing apps from pypi

### Added

## [1.1.0] - 2022-02-01

### Added

- Links to app pages now render with details about the app including title, description and icon when posted on Discord or other social media
- Page title now included in title of website as shown on browser tabs

### Changed

- More consistent titles for all pages

## [1.0.0a4] - 2021-11-04

### Added

- Ability to show full list of apps without paging

### Changed

- Replace own test tools with test tools from eve_auth for permissions

### Fixed

- Outdated version in `requirements.txt` fo django-esi
