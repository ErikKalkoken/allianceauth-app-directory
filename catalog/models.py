"""Models for catalog."""

import string
from typing import List, Optional

from packaging.requirements import Requirement

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models, transaction
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from .app_settings import blacklist
from .core import pypi
from .managers import AppManager, CategoryManager, ClassifierManager

MAX_LENGTH_CHAR_FIELDS = 254
MAX_LENGTH_URL_FIELDS = 1024


class App(models.Model):
    """An app for Alliance Auth."""

    class ContentType(models.TextChoices):
        """A content type."""

        UNKNOWN = "UN", "unknown"
        MARKDOWN = "MD", "text/markdown"
        PLAIN = "PL", "text/plain"

        @classmethod
        def from_pypi_type(cls, name):
            """Create from a PyPI type name."""
            try:
                idx = cls.labels.index(name)
            except ValueError:
                return cls.UNKNOWN
            return cls(cls.values[idx])

    category = models.ForeignKey(
        "Category",
        on_delete=models.CASCADE,
        related_name="apps",
        help_text="Category this app belongs to.",
    )
    created_on = models.DateTimeField(auto_now_add=True, blank=True)
    is_verified = models.BooleanField(
        default=False, help_text="App ownership has been verified."
    )
    maintainers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name="apps",
        blank=True,
        help_text="Users who maintain this app.",
    )
    icon = models.ImageField(
        upload_to="uploads/",
        default="",
        blank=True,
        help_text="Image to show as icon for this app. Max file size is 200 KB.",
    )
    name = models.CharField(
        max_length=MAX_LENGTH_CHAR_FIELDS,
        unique=True,
        help_text="Verbose name of this app.",
    )
    pypi_name = models.CharField(
        max_length=MAX_LENGTH_CHAR_FIELDS,
        unique=True,
        help_text="Name of the related distribution package on PyPI.",
    )
    updated_on = models.DateTimeField(auto_now=True, blank=True)

    # fields updated from PyPI
    authors = models.ManyToManyField("Author", related_name="apps", blank=True)
    classifiers = models.ManyToManyField("Classifier", related_name="apps")

    description = models.TextField()
    description_content_type = models.CharField(
        max_length=2, choices=ContentType.choices, default=ContentType.UNKNOWN
    )
    first_published = models.DateTimeField(default=None, null=True)
    homepage_url = models.URLField(max_length=MAX_LENGTH_URL_FIELDS, default="")
    last_published = models.DateTimeField(default=None, null=True)
    license = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS, default="")
    requirements = models.JSONField(default=None, null=True)
    requires = models.ManyToManyField("self", symmetrical=False, related_name="used_by")
    summary = models.TextField()
    version = models.CharField(
        max_length=MAX_LENGTH_CHAR_FIELDS, default=None, null=True
    )

    objects = AppManager()

    class Meta:
        permissions = [("can_confirm_ownership", "Can confirm app ownership")]

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        """Return absolute URL for this app."""
        return reverse("app-detail", kwargs={"slug": self.pypi_name})

    def clean(self):
        """Clean form data for this app."""
        if not self.id:
            self.pypi_name = slugify(self.pypi_name)
            if not self.update_from_pypi(save_model=False):
                raise ValidationError(
                    {
                        "pypi_name": [
                            _(
                                "Failed to fetch data from PyPI. "
                                "Are you sure the name is correct?"
                            ),
                        ]
                    }
                )
        if self.icon:
            if self.icon.size > 200 * 1024:
                raise ValidationError(
                    {
                        "icon": [
                            _("File is too large. Max allowed size is 200 KB."),
                        ]
                    }
                )
        if self.pypi_name:
            b = blacklist()
            if self.pypi_name in b:
                raise ValidationError(
                    {
                        "pypi_name": [
                            _("Can not add blacklisted app."),
                        ]
                    }
                )

    @cached_property
    def description_html(self):
        """Return description as HTML."""
        if self.description_content_type == self.ContentType.MARKDOWN:
            return pypi.markdown_to_html(self.description)
        return pypi.unknown_to_html(self.description)

    @property
    def pypi_url(self):
        """Return URL for project page on PyPI."""
        return f"https://pypi.org/project/{self.pypi_name}/"

    @property
    def requirements_count(self) -> Optional[int]:
        """Return count of requirements or None if no requirements defined."""
        return len(self.requirements) if self.requirements is not None else None

    @property
    def has_requirements(self) -> bool:
        """Return True, if this app has known requirements, else False."""
        return bool(self.requirements)

    @property
    def fake_icon_url(self) -> str:
        """Return fake icon URL for this app."""
        char = self.name[0:1].lower()
        char = char if char in string.ascii_lowercase else "a"
        return staticfiles_storage.url(f"/catalog/img/fake_icons/icon_{char}.png")

    @property
    def valid_icon_url(self) -> str:
        """Return icon ULR if it exists or else the fake icon URL."""
        if self.icon:
            return self.icon.url
        return self.fake_icon_url

    def update_from_pypi(self, save_model=True) -> bool:
        """Update the PyPI related data for this app from the PyPI website."""
        info = pypi.project_info(self.pypi_name)
        if not info:
            return False

        self.description = info.description
        self.description_content_type = self.ContentType.from_pypi_type(
            info.description_content_type
        )
        self.homepage_url = info.homepage_url[:MAX_LENGTH_URL_FIELDS]
        self.first_published = info.first_published
        self.last_published = info.last_published
        self.license = info.license[:MAX_LENGTH_CHAR_FIELDS]
        self.summary = info.summary
        self.version = info.version[:MAX_LENGTH_CHAR_FIELDS]
        self.requirements = info.requirements

        if save_model:
            self.save()

        if self.pk:
            self.update_requires()

            author_objs = self._author_objs_from_info(info)
            if author_objs:
                self.authors.set(author_objs, clear=True)

            classifier_objs = Classifier.objects.get_or_create_many(info.classifiers)
            if classifier_objs:
                self.classifiers.set(classifier_objs, clear=True)

        return True

    def _author_objs_from_info(self, info: pypi.PypiProjectInfo) -> List["Author"]:
        author_objs = []
        for author in info.authors:
            author_name = author.name[:MAX_LENGTH_CHAR_FIELDS]
            author_obj, _ = Author.objects.get_or_create(
                name=author_name, defaults={"email": author.email}
            )
            author_objs.append(author_obj)
        return author_objs

    @transaction.atomic()
    def update_requires(self):
        """Update required apps."""
        if self.pk:
            self.requires.clear()
            if self.requirements:
                apps = {app.pypi_name: app for app in App.objects.exclude(pk=self.pk)}
                for req_str in self.requirements:
                    req = Requirement(req_str)
                    if req.name in apps:
                        self.requires.add(apps[req.name])

    def requirement_list(self) -> List[dict]:
        """List of this app's requirements"""
        app_names = set(App.objects.values_list("pypi_name", flat=True))
        if not self.requirements:
            return []
        requirements = []
        for req_str in self.requirements:
            req = Requirement(req_str)
            requirements.append(
                {
                    "name": req.name,
                    "full": str(req),
                    "is_known": req.name in app_names,
                }
            )
        return requirements


class Author(models.Model):
    """An author of an app."""

    name = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS, unique=True)
    email = models.EmailField(default="")

    def __str__(self) -> str:
        return self.name


class Category(models.Model):
    """A category of an app."""

    class ContextStyle(models.TextChoices):
        """A context style for a category."""

        PRIMARY = ("primary", "primary")
        SECONDARY = ("secondary", "secondary")
        SUCCESS = ("success", "success")
        DANGER = ("danger", "danger")
        WARNING = ("warning", "warning")
        INFO = ("info", "info")
        LIGHT = ("light", "light")
        DARK = ("dark", "dark")

    created_on = models.DateTimeField(auto_now_add=True, blank=True)
    is_default = models.BooleanField(
        default=False,
        help_text="The default category will be preselected when adding apps",
    )
    name = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS, unique=True)
    summary = models.CharField(
        max_length=MAX_LENGTH_CHAR_FIELDS, blank=True, default=""
    )
    updated_on = models.DateTimeField(auto_now_add=True, blank=True)
    verbose_name = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS, db_index=True)
    context_style = models.CharField(
        max_length=16, choices=ContextStyle.choices, default=ContextStyle.SECONDARY
    )

    objects = CategoryManager()

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self) -> str:
        return self.verbose_name

    def save(self, *args, **kwargs) -> None:
        self.name = slugify(self.verbose_name)
        return super().save(*args, **kwargs)


class Classifier(models.Model):
    """A PyPI classifier."""

    name = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS, unique=True)

    objects = ClassifierManager()

    def __str__(self) -> str:
        return self.name


class Review(models.Model):
    """Review for an app"""

    app = models.ForeignKey(App, on_delete=models.CASCADE, related_name="reviews")
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True)
    details = models.TextField(blank=True)
    rating = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)], db_index=True
    )
    summary = models.CharField(max_length=MAX_LENGTH_CHAR_FIELDS)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["app", "author"],
                name="functional_pk_review",
            )
        ]

    def __str__(self) -> str:
        return f"{self.app}-{self.pk}"

    def get_absolute_url(self):
        """Return absolute URL for a review."""
        url = reverse("app-detail", kwargs={"slug": self.app.pypi_name})
        return f"{url}?tab=reviews"


class ReviewReply(models.Model):
    """Reply to a review."""

    review = models.OneToOneField(
        Review, on_delete=models.CASCADE, related_name="reply"
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    created_on = models.DateTimeField(auto_now_add=True)
    details = models.TextField(verbose_name="Reply")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["review", "author"],
                name="functional_pk_reviewreply",
            )
        ]

    def __str__(self) -> str:
        return f"{self.review}"

    def get_absolute_url(self):
        """Return absolute URL for a review reply."""
        url = reverse("app-detail", kwargs={"slug": self.review.app.pypi_name})
        return f"{url}?tab=reviews"
