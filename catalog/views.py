"""Views for catalog."""

import csv
import logging
from typing import Any
from urllib.parse import parse_qs, urlencode, urlparse

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, F, Q, TextChoices
from django.db.models.functions import Lower
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic.base import TemplateView, View
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from .forms import AppCreateForm, AppUpdateForm, ReviewForm, ReviewReplyForm
from .models import App, Author, Category, Review, ReviewReply

logger = logging.getLogger(__name__)
User = get_user_model()


class SpecialCategory(TextChoices):
    """App category for filtering app lists."""

    ALL_APPS = "all", "All Apps"
    MY_APPS = "my-apps", "My Apps"
    TOP_RATED = "top-rated", "Top Rated"
    NEW_APPS = "new-apps", "Newly added"
    AA4 = "aa4", "AA 4 compatible"
    UNVERIFIED = "unverified", "Unverified"

    @property
    def subtitle(self) -> str:
        """Return category as subtitle."""
        my_map = {self.TOP_RATED: "Top 10 rated apps"}
        return my_map[self] if self in my_map else ""


class AppConfirmVerificationView(LoginRequiredMixin, SingleObjectMixin, View):
    """Confirm app ownership is verified."""

    model = App

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.object = None

    def get(self, request, *args, **kwargs):
        """::private::"""
        self.object = self.get_object()
        self.object.is_verified = True
        self.object.save()
        return HttpResponseRedirect(self.object.get_absolute_url())


class AddPageTitleMixin:
    """Mixing for adding the page title to a view."""

    def get_context_data(self, **kwargs):
        """Add data to the context."""
        context = super().get_context_data(**kwargs)
        try:
            context["page_title"] = self.page_title
        except AttributeError:
            pass
        return context


class IndexView(AddPageTitleMixin, TemplateView):
    """View to render the index page."""

    template_name = "catalog/index.html"
    page_title = "Home"

    def get_context_data(self, **kwargs):
        """Add data to the context."""
        context = super().get_context_data(**kwargs)
        context["all_apps_count"] = App.objects.count()
        return context


class FaqView(AddPageTitleMixin, TemplateView):
    """View to render the FAQ page."""

    template_name = "catalog/faq.html"
    page_title = "FAQ"


class DependenciesView(AddPageTitleMixin, TemplateView):
    """View to render the dependencies page."""

    template_name = "catalog/dependencies.html"
    page_title = "Dependencies"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        apps = (
            App.objects.filter(Q(used_by__isnull=False) | Q(requires__isnull=False))
            .exclude(is_verified=False)
            .distinct()
            .select_related("category")
            .prefetch_related("requires")
        )
        nodes = [
            {
                "id": app.pk,
                "label": app.name,
                "title": app.summary,
                "group": app.category.context_style,
            }
            for app in apps
        ]
        edges = []
        for app in apps:
            for other_app in app.requires.all():
                edges.append({"from": app.pk, "to": other_app.pk})

        context["nodes"] = nodes
        context["edges"] = edges
        context["urls"] = {app.pk: app.get_absolute_url() for app in apps}
        context["style_names"] = Category.ContextStyle.labels
        try:
            context["highlight_style"] = str(settings.DEPENDENCIES_HIGHLIGHT_STYLE)
        except AttributeError:
            context["highlight_style"] = "warning"
        context["categories"] = Category.objects.filter(apps__in=list(apps)).distinct()
        return context


class AppListView(ListView):
    """Render list of apps with different filters applied."""

    model = App
    paginate_by = 25

    def dispatch(self, request, *args, **kwargs):
        """Redirect to default category if nothing else is specified."""
        author = self.request.GET.get("author")
        category = self.kwargs.get("category")
        maintainer = self.request.GET.get("maintainer")
        search = self.request.GET.get("search")
        if not author and not category and not maintainer and not search:
            return redirect(reverse("app-list", args=[SpecialCategory.ALL_APPS]))

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        apps_qs = (
            super()
            .get_queryset()
            .user_can_access(self.request.user)
            .annotate_rating_avg()
        )

        author = self.request.GET.get("author")
        category = self.kwargs.get("category")
        maintainer = self.request.GET.get("maintainer")
        search = self.request.GET.get("search")
        if search:
            apps_qs = (
                apps_qs.filter(summary__icontains=search)
                | apps_qs.filter(name__icontains=search)
                | apps_qs.filter(description__icontains=search)
                | apps_qs.filter(authors__name__icontains=search)
                | apps_qs.filter(
                    maintainers__eve_character__character_name__icontains=search
                )
            )

        elif author:
            apps_qs = apps_qs.filter(authors__name=author)

        elif category in SpecialCategory:
            apps_qs = self._qs_add_special_category(apps_qs, category)

        elif maintainer:
            apps_qs = apps_qs.filter(maintainers__username=maintainer)

        else:
            apps_qs = apps_qs.filter(category__name=category)

        if category and category == SpecialCategory.TOP_RATED:
            apps_qs = apps_qs.order_by("-rating_avg")[:10]
        elif category and category == SpecialCategory.NEW_APPS:
            apps_qs = apps_qs.order_by("-created_on")
        else:
            apps_qs = apps_qs.order_by(Lower("name"))
        return apps_qs

    def _qs_add_special_category(self, apps_qs, category):
        if category == SpecialCategory.ALL_APPS:
            return apps_qs.all()

        if category == SpecialCategory.TOP_RATED:
            return apps_qs.filter(rating_avg__gt=1)

        if category == SpecialCategory.NEW_APPS:
            return apps_qs.filter_new_apps()

        if self.request.user.is_authenticated and category == SpecialCategory.MY_APPS:
            return apps_qs.filter_user_apps(self.request.user)

        if category == SpecialCategory.UNVERIFIED:
            return apps_qs.filter_unverified()

        if category == SpecialCategory.AA4:
            return apps_qs.filter_aa4_compatible()

        return apps_qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        title, subtitle = self._calc_titles()
        context["page_title"] = title
        context["page_subtitle"] = subtitle

        context["SpecialCategory"] = SpecialCategory
        page_obj = context["page_obj"]
        context["page_range"] = page_obj.paginator.get_elided_page_range(
            page_obj.number, on_each_side=2
        )
        context["show_all"] = "all" in self.request.GET
        context["show_all_url"] = self._build_show_all_url()

        context["all_apps_count"] = App.objects.count()
        context["new_apps_count"] = App.objects.filter_new_apps().count()
        context["aa4_apps_count"] = App.objects.filter_aa4_compatible().count()
        context["unverified_apps_count"] = App.objects.filter_unverified().count()
        my_apps_count = (
            App.objects.filter_user_apps(self.request.user).count()
            if self.request.user.is_authenticated
            else None
        )
        context["my_apps_count"] = my_apps_count

        context["categories"] = (
            Category.objects.filter(apps__isnull=False)
            .annotate(app_count=Count("apps"))
            .distinct()
        )

        authors = (
            Author.objects.filter(apps__isnull=False)
            .annotate(app_count=Count("apps"))
            .order_by(Lower("name"))
            .distinct()
        )
        context["authors"] = authors
        maintainers = (
            User.objects.filter(apps__isnull=False)
            .annotate(app_count=Count("apps"))
            .annotate(character_name=F("eve_character__character_name"))
            .order_by(Lower("eve_character__character_name"))
            .distinct()
        )
        context["maintainers"] = maintainers
        return context

    def _calc_titles(self):
        title = None
        subtitle = None
        author = self.request.GET.get("author")
        maintainer = self.request.GET.get("maintainer")
        search = self.request.GET.get("search")

        if search:
            title = f'Results for "{search}"'

        elif author:
            author = get_object_or_404(Author, name=author)
            title = f"Apps authored by {author.name}"

        elif maintainer:
            user = get_object_or_404(User, username=maintainer)
            maintainer_name = user.eve_character.character_name
            title = f"Apps maintained by {maintainer_name}"

        else:
            category = self.kwargs.get("category", "")
            if category in SpecialCategory.values:
                special_category = SpecialCategory(category)
                title = special_category.label
                subtitle = special_category.subtitle
                if special_category is SpecialCategory.MY_APPS:
                    subtitle = f"Apps maintained by {self.request.user.eve_character}"
            else:
                try:
                    category_obj = Category.objects.get(name=category)
                except Category.DoesNotExist:
                    pass
                else:
                    title = category_obj.verbose_name
                    subtitle = category_obj.summary
        return title, subtitle

    def _build_show_all_url(self) -> str:
        parts = urlparse(self.request.get_full_path())
        query = parse_qs(parts.query)
        query["all"] = 1
        params = urlencode(query=query, doseq=True)
        return f"{self.request.path}?{params}"


class AppDetailView(DetailView):
    """Render detail view of an app."""

    model = App
    slug_field = "pypi_name"

    def get_queryset(self):
        qs = (
            super()
            .get_queryset()
            .user_can_access(self.request.user)
            .select_related("category")
            .prefetch_related("maintainers", "maintainers__eve_character")
            .annotate_rating_avg()
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = self.object.name
        context["reviews"] = self.object.reviews.select_related(
            "author", "author__eve_character"
        )
        context["requirements"] = self.object.requirement_list()
        context["classifiers"] = self.object.classifiers.order_by("name").values_list(
            "name", flat=True
        )
        context["used_by_apps"] = self.object.used_by.order_by(Lower("name"))
        context["requires_apps"] = self.object.requires.order_by(Lower("name"))
        context["depencies_count"] = (
            self.object.requires.count() + self.object.used_by.count()
        )
        context["user_has_review"] = (
            self.request.user.is_authenticated
            and self.object.reviews.filter(author=self.request.user).exists()
        )
        context["tab"] = self.request.GET.get("tab")
        return context


class AppCreateView(AddPageTitleMixin, LoginRequiredMixin, CreateView):
    """Render view for create a new app."""

    model = App
    form_class = AppCreateForm
    page_title = "Add new app"

    def get_initial(self):
        default_category = Category.objects.get_default()
        return {"category": default_category}

    def form_valid(self, form):
        res = super().form_valid(form)
        form.instance.maintainers.add(self.request.user)
        return res


class AppUpdateView(AddPageTitleMixin, LoginRequiredMixin, UpdateView):
    """Render view for updating an existing app."""

    model = App
    form_class = AppUpdateForm
    slug_field = "pypi_name"
    page_title = "Update existing app"

    def get_queryset(self):
        """Return only apps that the current user is a maintainer."""
        qs = super().get_queryset().user_can_access(self.request.user)
        return qs.filter(
            id__in=list(self.request.user.apps.values_list("id", flat=True))
        )


class AppDeleteView(AddPageTitleMixin, LoginRequiredMixin, DeleteView):
    """Render view for deleting an app after confirmation."""

    model = App
    slug_field = "pypi_name"
    success_url = reverse_lazy("app-list")
    page_title = "Delete app"

    def get_queryset(self):
        """Return only apps that the current user has created."""
        qs = super().get_queryset().user_can_access(self.request.user)
        return qs.filter(
            id__in=list(self.request.user.apps.values_list("id", flat=True))
        )


class ReviewCreateView(AddPageTitleMixin, LoginRequiredMixin, CreateView):
    """Render view create a new review for an app."""

    model = Review
    form_class = ReviewForm
    page_view = "Add review"

    def get_initial(self):
        app = get_object_or_404(App, pypi_name=self.kwargs.get("slug"))
        return {"app": app}

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["app"] = get_object_or_404(App, pypi_name=self.kwargs.get("slug"))
        return context


class ReviewDeleteView(AddPageTitleMixin, LoginRequiredMixin, DeleteView):
    """Render view for deleting a review after confirmation."""

    model = Review
    page_title = "Delete review"

    def get_success_url(self) -> str:
        url = reverse("app-detail", args=[self.object.app.pypi_name])
        return f"{url}?tab=reviews"

    def get_queryset(self):
        """Return only reviews that the current user has created."""
        qs = super().get_queryset()
        return qs.filter(author=self.request.user)


class ReviewReplyCreateView(AddPageTitleMixin, LoginRequiredMixin, CreateView):
    """Render view for creatign reply to a review."""

    model = ReviewReply
    form_class = ReviewReplyForm
    page_title = "Reply to review"

    def get_initial(self):
        review = get_object_or_404(Review, pk=self.kwargs.get("pk"))
        return {"review": review}

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["review"] = get_object_or_404(Review, pk=self.kwargs.get("pk"))
        return context


class ReviewReviewDeleteView(AddPageTitleMixin, LoginRequiredMixin, DeleteView):
    """Render view for deleting a review reply with confirmation."""

    model = ReviewReply
    page_title = "Delete reply"

    def get_success_url(self) -> str:
        url = reverse("app-detail", args=[self.object.review.app.pypi_name])
        return f"{url}?tab=reviews"

    def get_queryset(self):
        """Return only replies that the current user has created."""
        qs = super().get_queryset()
        return qs.filter(author=self.request.user)


@staff_member_required
def export_data(request):
    """Export all apps into a CSV file for direct download."""
    response = HttpResponse(
        content_type="text/csv",
        headers={"Content-Disposition": 'attachment; filename="allianceauth_apps.csv"'},
    )
    writer = csv.writer(response)
    queryset = App.objects.filter(is_verified=True)
    excluded_fields = {
        "description",
        "description_content_type",
        "icon",
        "is_verified",
        "requirements",
    }
    field_names = [
        field.name
        for field in queryset.model._meta.fields
        if field.name not in excluded_fields
    ]
    writer.writerow(field_names)
    for obj in queryset:
        writer.writerow([getattr(obj, field) for field in field_names])
    return response
