"""Settings."""

from typing import Set

from django.conf import settings


def blacklist() -> Set[str]:
    """Return pypi names of blacklisted apps."""
    if not hasattr(settings, "BLACKLIST"):
        return []
    return set(settings.BLACKLIST)
