"""Management command to run batch for catalog."""

import logging

from django.core.management.base import BaseCommand, CommandError

from catalog.core import community_creations, pypi
from catalog.models import App, Category

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """A Django management command to run the batch."""

    help = "Run appdir batch."

    def add_arguments(self, parser):
        parser.add_argument(
            "--silent", action="store_true", help="Disable console output"
        )

        parser.add_argument(
            "--pypi-only", action="store_true", help="Only import from PyPI"
        )

    def create_apps_from_community_list(self, **options):
        """Create new apps from the official community creations list."""
        if not options["silent"]:
            self.stdout.write("Checking community creations list for new apps...")

        pypi_projects = pypi.projects()
        known_app_projects = set(App.objects.values_list("pypi_name", flat=True))
        default_category = Category.objects.get_default()
        created_count = 0
        for repo in community_creations.repositories():
            if repo.name not in known_app_projects and repo.name in pypi_projects:
                app, _ = App.objects.get_or_create(
                    pypi_name=repo.name,
                    defaults={"name": repo.name, "category": default_category},
                )
                app.update_from_pypi()
                created_count += 1

        if not options["silent"]:
            if created_count:
                self.stdout.write(
                    f"Created {created_count} new apps from the community list."
                )
            else:
                self.stdout.write("No new apps found.")

    def update_apps_from_pypi(self, **options):
        """Update existing apps with new infos from PyPI."""
        apps_qs = App.objects.exclude(pypi_name="").exclude(category__name="demo-apps")

        if not options["silent"]:
            self.stdout.write(f"Updating {apps_qs.count()} apps from PyPI...")

        apps_qs.bulk_update_from_pypi()

        if not options["silent"]:
            self.stdout.write(self.style.SUCCESS("App update completed."))

    def handle(self, *args, **options):
        if not Category.objects.exists():
            raise CommandError("You need to create at least one category.")

        if not options["pypi_only"]:
            self.create_apps_from_community_list(**options)

        self.update_apps_from_pypi(**options)
