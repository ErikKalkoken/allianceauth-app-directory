"""Generate fake icons for all letters of the alphabet and store them as static file."""

import random
import string
from pathlib import Path

from PIL import Image, ImageDraw, ImageFont

SIZE = 80
icon_path = Path(__file__).parent.parent / "static" / "catalog" / "img" / "fake_icons"

colors = [(224, 242, 241), (227, 242, 253), (232, 234, 246), (255, 235, 238)]
my_font = ImageFont.truetype(str(Path(__file__).parent / "NotoSans-Regular.ttf"), 36)
for character in string.ascii_uppercase:
    img = Image.new("RGB", (SIZE, SIZE), color=random.choice(colors))
    draw = ImageDraw.Draw(img)
    draw.text(
        (SIZE / 2, SIZE / 2), str(character), font=my_font, fill="black", anchor="mm"
    )
    img.save(icon_path / f"icon_{character.lower()}.png")
