import requests_mock

from django.test import TestCase, override_settings

from catalog.core import community_creations

PACKAGE_PATH = "appdir.catalog.core"

md_text = """
# Repos

## Alpha
- [colcrunch / fittings](https://gitlab.com/colcrunch/fittings) - abc.
- [Erik Kalkoken / aa-freight](https://gitlab.com/ErikKalkoken/aa-freight.git) - efg

## Bravo
- [Erik Kalkoken / aa-moonmining](https://gitlab.com/ErikKalkoken/aa-moonmining) - hij
"""


@requests_mock.Mocker()
class TestCommunityCreations(TestCase):
    def test_should_identify_all_repos(self, request_mocker):
        # given
        request_mocker.register_uri(
            "GET",
            "https://gitlab.com/allianceauth/community-creations/-/raw/master/README.md",
            text=md_text,
        )
        # when
        result = community_creations.repositories()
        # then
        repos = {obj.name: obj for obj in result}
        self.assertSetEqual(
            set(repos.keys()), {"fittings", "aa-freight", "aa-moonmining"}
        )

    @override_settings(BLACKLIST=["aa-freight"])
    def test_should_ignore_repos_from_blacklist(self, request_mocker):
        # given
        request_mocker.register_uri(
            "GET",
            "https://gitlab.com/allianceauth/community-creations/-/raw/master/README.md",
            text=md_text,
        )
        # when
        result = community_creations.repositories()
        # then
        repos = {obj.name: obj for obj in result}
        self.assertSetEqual(set(repos.keys()), {"fittings", "aa-moonmining"})
