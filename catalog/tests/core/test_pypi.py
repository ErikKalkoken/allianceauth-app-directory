import datetime as dt
from copy import deepcopy

import requests_mock
from pytz import utc

from django.test import TestCase, override_settings

from catalog.core import pypi
from catalog.tests.testdata.load_pypi import pypi_projects

MODULE_PATH = "catalog.core.pypi"


@override_settings(AUTHOR_MAPPING={"Erik Kalkoken": ["erik"]})
class TestPyPiAuthor(TestCase):
    def test_should_create_from_old_variant(self):
        # given
        info = {"author": "Erik Kalkoken", "author_email": "kalkoken87@gmail.com"}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        obj = result[0]
        self.assertEqual(obj.name, "Erik Kalkoken")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")

    def test_should_create_from_new_variant_1(self):
        # given
        info = {"author": "", "author_email": "Erik Kalkoken <kalkoken87@gmail.com>"}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        self.assertEqual(len(result), 1)
        obj = result[0]
        self.assertEqual(obj.name, "Erik Kalkoken")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")

    def test_should_create_from_new_variant_2(self):
        # given
        info = {
            "author": "",
            "author_email": (
                "Erik Kalkoken <kalkoken87@gmail.com>, "
                "Peter Pfeufer <develop@ppfeufer.de>, "
                "Rebecca Murphy <rebecca@rcmurphy.me>"
            ),
        }
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        self.assertEqual(len(result), 3)

        obj = result[0]
        self.assertEqual(obj.name, "Erik Kalkoken")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")

        obj = result[1]
        self.assertEqual(obj.name, "Peter Pfeufer")
        self.assertEqual(obj.email, "develop@ppfeufer.de")

        obj = result[2]
        self.assertEqual(obj.name, "Rebecca Murphy")
        self.assertEqual(obj.email, "rebecca@rcmurphy.me")

    def test_should_create_from_new_variant_3(self):
        # given
        info = {"author": "", "author_email": "kalkoken87@gmail.com"}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        self.assertEqual(len(result), 1)
        obj = result[0]
        self.assertEqual(obj.name, "kalkoken87")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")

    def test_should_create_from_new_variant_4(self):
        # given
        info = {"author": "", "author_email": ""}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        self.assertListEqual(result, [])

    def test_should_map_author_name_old_variant(self):
        # given
        info = {"author": "erik", "author_email": "kalkoken87@gmail.com"}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        obj = result[0]
        self.assertEqual(obj.name, "Erik Kalkoken")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")

    def test_should_map_author_name_new_variant(self):
        # given
        info = {"author": "", "author_email": "erik <kalkoken87@gmail.com>"}
        # when
        result = pypi.PyPiProjectAuthor.create_list_from_info(info)
        # then
        obj = result[0]
        self.assertEqual(obj.name, "Erik Kalkoken")
        self.assertEqual(obj.email, "kalkoken87@gmail.com")


class TestPyPiProjectInfo(TestCase):
    def test_should_create_from_variant_a_info(self):
        # given
        info = pypi_projects["variant_a"]["info"]
        # when
        result = pypi.PypiProjectInfo.create_from_pypi_info(info, None, None)
        # then
        self.assertEqual(
            result.authors,
            [pypi.PyPiProjectAuthor("Erik Kalkoken", "kalkoken87@gmail.com")],
        )
        self.assertEqual(result.description, "description placeholder")
        self.assertEqual(result.description_content_type, "text/markdown")
        self.assertIsNone(result.first_published)
        self.assertEqual(
            result.homepage_url, "https://gitlab.com/ErikKalkoken/aa-structures"
        )
        self.assertEqual(result.license, "MIT License")
        self.assertEqual(result.name, "aa-structures")
        self.assertListEqual(
            result.requirements,
            [
                "allianceauth (>=2.9)",
                "django-eveuniverse (>=0.16)",
                "allianceauth-app-utils (>=1.13)",
            ],
        )
        self.assertEqual(
            result.summary, "App for managing Eve Online structures with Alliance Auth"
        )
        self.assertEqual(result.version, "1.12.0")

    def test_should_create_from_variant_b_info(self):
        # given
        info = pypi_projects["variant_b"]["info"]
        # when
        result = pypi.PypiProjectInfo.create_from_pypi_info(info, None, None)
        # then
        self.assertEqual(
            result.authors,
            [pypi.PyPiProjectAuthor("Erik Kalkoken", "kalkoken87@gmail.com")],
        )
        self.assertEqual(result.description, "description placeholder")
        self.assertEqual(result.description_content_type, "text/markdown")
        self.assertIsNone(result.first_published)
        self.assertEqual(
            result.homepage_url, "https://gitlab.com/ErikKalkoken/aa-structures"
        )
        self.assertIsNone(result.last_published)
        self.assertEqual(result.license, "MIT License")
        self.assertEqual(result.name, "aa-structures")
        self.assertListEqual(
            result.requirements,
            [
                "allianceauth-app-utils>=1.22",
                "allianceauth>=3.0",
                "dhooks-lite>=1.0",
                "django-eveuniverse>=1.5.2",
                "django-multiselectfield",
                "django-navhelper",
                "pytz!=2022.2",
                "redis-simple-mq>=1.0",
                "humanize>=4.7",
            ],
        )
        self.assertEqual(
            result.summary, "App for managing Eve Online structures with Alliance Auth."
        )
        self.assertEqual(result.version, "2.7.0")
        self.assertListEqual(
            result.classifiers,
            [
                "Environment :: Web Environment",
                "Framework :: Django",
                "Framework :: Django :: 4.0",
                "Intended Audience :: End Users/Desktop",
                "License :: OSI Approved :: MIT License",
                "Operating System :: OS Independent",
                "Programming Language :: Python",
                "Programming Language :: Python :: 3.10",
                "Programming Language :: Python :: 3.11",
                "Programming Language :: Python :: 3.8",
                "Programming Language :: Python :: 3.9",
                "Topic :: Internet :: WWW/HTTP",
                "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
            ],
        )

    def test_can_handle_null_summary(self):
        # given
        info = deepcopy(pypi_projects["variant_a"]["info"])
        info["summary"] = None
        # when
        result = pypi.PypiProjectInfo.create_from_pypi_info(info, None, None)
        # then
        self.assertEqual(result.summary, "")


class TestPyPiProjectInfoExtractLicense(TestCase):
    def test_should_extract_license_from_info(self):
        # given
        info = pypi_projects["variant_a"]["info"]
        # when
        result = pypi.PypiProjectInfo.extract_license_from_pypi_info(info)
        # then
        self.assertEqual(result, "MIT License")

    def test_should_extract_license_from_info_when_empty_1(self):
        # given
        info = {"classifiers": []}
        # when
        result = pypi.PypiProjectInfo.extract_license_from_pypi_info(info)
        # then
        self.assertEqual(result, "")

    def test_should_extract_license_from_info_when_empty_2(self):
        # given
        info = {"classifiers": None}
        # when
        result = pypi.PypiProjectInfo.extract_license_from_pypi_info(info)
        # then
        self.assertEqual(result, "")

    def test_should_extract_license_variants(self):
        # given
        cases = [
            (
                1,
                "License :: OSI Approved :: MIT License",
                "MIT License",
            ),
            (
                2,
                "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
                "GPLv3",
            ),
        ]
        for num, classifier, expected in cases:
            with self.subTest(num=num):
                # when
                info = {"classifiers": [classifier]}
                result = pypi.PypiProjectInfo.extract_license_from_pypi_info(info)
                # then
                self.assertEqual(result, expected)


class TestPyPi(TestCase):
    @requests_mock.Mocker()
    def test_should_return_infos_for_variant_a(self, request_mocker):
        # given
        request_mocker.register_uri(
            "GET", "https://pypi.org/pypi/dummy/json", json=pypi_projects["variant_a"]
        )
        # when
        result = pypi.project_info("dummy")
        # then
        self.assertEqual(
            result.first_published, dt.datetime(2020, 2, 12, 14, 18, 24, tzinfo=utc)
        )
        self.assertEqual(
            result.last_published, dt.datetime(2021, 5, 22, 16, 31, 12, tzinfo=utc)
        )

    @requests_mock.Mocker()
    def test_should_return_infos_for_variant_b(self, request_mocker):
        # given
        request_mocker.register_uri(
            "GET", "https://pypi.org/pypi/dummy/json", json=pypi_projects["variant_b"]
        )
        # when
        result = pypi.project_info("dummy")
        # then
        self.assertEqual(
            result.first_published, dt.datetime(2020, 2, 12, 14, 18, 24, tzinfo=utc)
        )
        self.assertEqual(
            result.last_published, dt.datetime(2023, 11, 16, 0, 40, tzinfo=utc)
        )

    def test_should_convert_markdown_to_html(self):
        # when
        result = pypi.markdown_to_html(
            "# Structures\n![picture](https://www.example.com/dog.jpg)"
        )
        # then
        self.assertEqual(
            result,
            (
                '<h1>Structures</h1>\n<p><img class="img-fluid" alt="picture" '
                'src="https://www.example.com/dog.jpg"></p>'
            ),
        )

    def test_should_convert_markdown_to_html_with_xss_protection(self):
        # when
        result = pypi.markdown_to_html("<script>evil()</script>")
        # then
        self.assertEqual(result, "&lt;script&gt;evil()&lt;/script&gt;")

    @requests_mock.Mocker()
    def test_should_return_all_projects(self, request_mocker):
        # given
        html = """
            <body>
                <a href="#">Alpha</a>
                <a href="#">Bravo</a>
                <a href="#">Charlie</a>
            </body>
        """
        request_mocker.register_uri("GET", "https://pypi.org/simple/", text=html)
        # when
        result = pypi.projects()
        # then
        self.assertSetEqual(result, {"Alpha", "Bravo", "Charlie"})
