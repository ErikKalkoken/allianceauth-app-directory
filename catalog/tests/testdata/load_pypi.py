import datetime as dt
import json
from pathlib import Path

from django.utils.timezone import now

from catalog.core.pypi import PypiProjectInfo


def _load_pypi_projects() -> dict:
    pypi_file = Path(__file__).parent / "pypi_projects.json"
    with pypi_file.open("r", encoding="utf-8") as fp:
        return json.load(fp)


def gen_pypi_info(
    first_published: dt = None, last_published: dt = None
) -> PypiProjectInfo:
    obj = PypiProjectInfo.create_from_pypi_info(
        info=pypi_projects["variant_a"]["info"],
        first_published=first_published if first_published else now(),
        last_published=last_published if last_published else now(),
    )
    return obj


pypi_projects = _load_pypi_projects()
pypi_info = gen_pypi_info()
