"""Fetching information from the PyPI API."""

import datetime as dt
import logging
import re
from typing import List, NamedTuple, Optional, Set, Tuple

import bleach
import lxml.html
import markdown
import requests
from pytz import utc

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.dateparse import parse_datetime
from django.utils.safestring import mark_safe

logger = logging.getLogger(__name__)

BLEACH_ALLOWED_TAGS = bleach.sanitizer.ALLOWED_TAGS | {
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "img",
    "p",
    "pre",
    "table",
    "tbody",
    "thead",
    "td",
    "th",
    "tr",
}

BLEACH_ALLOWED_ATTRIBUTES = {
    **bleach.ALLOWED_ATTRIBUTES,
    **{"*": ["src", "alt", "class"]},
}
PYPI_ALL_REPOS = "https://pypi.org/simple/"


class PyPiProjectAuthor(NamedTuple):
    """An author of a PyPi project."""

    name: str
    email: str

    @classmethod
    def create_list_from_info(cls, info: dict) -> List["PyPiProjectAuthor"]:
        """Create list of new objects from info dict as received from the PyPI API."""

        if info["author"]:
            authors = cls._extract_author_old_variant(info)
        else:
            authors = cls._extract_author_new_variant(info)

        return cls._map_author_names(authors)

    @classmethod
    def _extract_author_old_variant(cls, info: dict) -> List["PyPiProjectAuthor"]:
        author_name = info["author"]
        author = cls(name=author_name, email=info.get("author_email", ""))
        return [author]

    @classmethod
    def _extract_author_new_variant(cls, info: dict) -> List["PyPiProjectAuthor"]:
        authors_raw = []
        author_email = info["author_email"]
        for part in author_email.split(","):
            match = re.match(r"([^<>]+)(<.+>)?", part)
            if match:
                name_match = match.group(1)
                email_match = match.group(2)
                name = name_match.strip() if name_match else ""
                email = email_match.strip("<>") if email_match else ""

                if name and not email:
                    try:
                        validate_email(name)
                    except ValidationError:
                        pass
                    else:
                        email = name
                        email_parts = email.split("@")
                        if email_parts:
                            name = email_parts[0]
                        else:
                            name = ""

                if name or email:
                    author = cls(name=name, email=email)
                    authors_raw.append(author)

        return authors_raw

    @classmethod
    def _map_author_names(
        cls, authors_raw: List["PyPiProjectAuthor"]
    ) -> List["PyPiProjectAuthor"]:
        authors_clean = []
        inverted_author_mapping = cls._create_inverted_author_mapping()
        for author in authors_raw:
            name = inverted_author_mapping.get(author.name, author.name)
            authors_clean.append(cls(name=name, email=author.email))
        return authors_clean

    @staticmethod
    def _create_inverted_author_mapping():
        inverted_map = {}
        for name, alternates in settings.AUTHOR_MAPPING.items():
            for alternate in alternates:
                inverted_map[alternate] = name

        return inverted_map


class PypiProjectInfo(NamedTuple):
    """Information about a project on PyPI."""

    authors: List[PyPiProjectAuthor]
    classifiers: List[str]
    description: str
    description_content_type: str
    first_published: Optional[dt.datetime]
    homepage_url: str
    last_published: Optional[dt.datetime]
    license: str
    name: str
    requirements: Optional[List[str]]
    summary: str
    version: str

    @classmethod
    def create_from_pypi_info(
        cls,
        info: dict,
        first_published: Optional[dt.datetime],
        last_published: Optional[dt.datetime],
    ) -> "PypiProjectInfo":
        """Create new object from info dict as received from the PyPI API."""
        obj = cls(
            authors=PyPiProjectAuthor.create_list_from_info(info),
            classifiers=info["classifiers"],
            description=info["description"],
            description_content_type=info["description_content_type"],
            first_published=first_published,
            homepage_url=cls.extract_homepage_from_pypi_info(info),
            last_published=last_published,
            license=cls.extract_license_from_pypi_info(info),
            name=info["name"],
            requirements=info["requires_dist"] if info.get("requires_dist") else None,
            summary=info["summary"] or "",
            version=info["version"],
        )
        return obj

    @staticmethod
    def extract_homepage_from_pypi_info(info: dict):
        """Return homepage URL from a PyPI info dict."""
        if homepage := info["home_page"]:
            return homepage

        for name, url in info["project_urls"].items():
            if name.lower() == "homepage":
                return url

        return ""

    @staticmethod
    def extract_license_from_pypi_info(info: dict) -> str:
        """Return license from a PyPI info dict."""
        for classifier in info.get("classifiers") or []:
            parts = classifier.split(" :: ")
            if parts and parts[0].lower() == "license":
                license_str = parts.pop()
                matches = re.findall(r"\((.+)\)", license_str)
                if matches:
                    return matches[0]
                return license_str

        return ""


def project_info(name: str) -> Optional[PypiProjectInfo]:
    """Fetch project info from PyPI and return in."""
    data = _fetch_from_pypi(name)
    if not data:
        return None

    info = data["info"]
    first_published, last_published = _calc_publish_dates(data)
    obj = PypiProjectInfo.create_from_pypi_info(
        info,
        first_published=utc.localize(first_published) if first_published else None,
        last_published=utc.localize(last_published) if last_published else None,
    )
    return obj


def _fetch_from_pypi(name: str) -> dict:
    response = requests.get(f"https://pypi.org/pypi/{name}/json", timeout=10)
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        logger.warning(
            "Failed to fetch project info from PyPI for %s", name, exc_info=True
        )
        return {}
    data = response.json()
    return data


def _calc_publish_dates(
    data: dict,
) -> Tuple[Optional[dt.datetime], Optional[dt.datetime]]:
    releases = list(data["releases"].values())
    upload_times = [
        parse_datetime(release[0]["upload_time"]) for release in releases if release
    ]
    if not upload_times:
        return None, None
    last_published = max(upload_times)
    first_published = min(upload_times)
    return first_published, last_published


def markdown_to_html(markdown_text: str) -> str:
    """Convert markdown into HTML and return it."""
    html = markdown.markdown(
        markdown_text, extensions=["extra", "pymdownx.superfences"]
    )
    html = bleach.clean(
        html, tags=BLEACH_ALLOWED_TAGS, attributes=BLEACH_ALLOWED_ATTRIBUTES
    )
    html = html.replace("<img ", '<img class="img-fluid" ')
    html = html.replace("<table>", '<table class="table">')
    html = html.replace("<a href=", '<a target="_blank" href=')
    return mark_safe(html)


def unknown_to_html(text: str) -> str:
    """Bleach HTML and return it."""
    html = bleach.clean(text, tags=BLEACH_ALLOWED_TAGS)
    return html


def projects() -> Set[str]:
    """All current PyPI project names"""
    response = requests.get(PYPI_ALL_REPOS, timeout=10)
    response.raise_for_status()
    root = lxml.html.fromstring(response.text)
    return {element.text for element in root.iter() if element.tag == "a"}
