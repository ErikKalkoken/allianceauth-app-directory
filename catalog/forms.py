"""Forms for catalog."""

# pylint: disable=missing-class-docstring

from django_starfield import Stars

from django import forms

from .models import App, Category, Review, ReviewReply


class ImageWidget(forms.widgets.ClearableFileInput):
    template_name = "catalog/widgets/image_widget.html"


class AppForm(forms.ModelForm):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.fields["category"].queryset = Category.objects.order_by("verbose_name")


class AppCreateForm(AppForm):
    confirmation = forms.BooleanField(
        label="I hereby confirm that I am a maintainer of this app."
    )

    class Meta:
        model = App
        fields = ("name", "pypi_name", "category", "icon", "confirmation")
        widgets = {"icon": ImageWidget}


class AppUpdateForm(AppForm):
    pypi_name = forms.CharField(
        max_length=255,
        disabled=True,
        help_text="Name of the related distribution package on PyPI.",
    )

    class Meta:
        model = App
        fields = ("name", "pypi_name", "category", "icon")
        widgets = {"icon": ImageWidget}


class ReviewForm(forms.ModelForm):
    rating = forms.IntegerField(widget=Stars)

    class Meta:
        model = Review
        fields = ("summary", "details", "rating", "app")
        widgets = {"app": forms.HiddenInput()}


class ReviewReplyForm(forms.ModelForm):
    class Meta:
        model = ReviewReply
        fields = ("details", "review")
        widgets = {"review": forms.HiddenInput()}
